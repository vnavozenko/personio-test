package com.personio.test.service

import com.personio.test.model.ErrorMessageCodes.EMPLOYEE_SUPERVISOR_CYCLIC_DEPENDENCY
import com.personio.test.model.ValidationException
import com.personio.test.repository.EmployeeRepository
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig

@SpringJUnitConfig(classes = [EmployeeService::class])
class EmployeeServiceTest {

    @MockBean
    lateinit var employeeRepository: EmployeeRepository

    @Autowired
    lateinit var employeeService: EmployeeService

    @Test
    fun `should throw validation exception when employee name and supervisor are the same`() {
        val employeeSupervisor = mapOf(
            "Pete" to "Nick",
            "Barbara" to "Nick",
            "Barbara" to "Barbara"
        )

        val message = assertThrows(ValidationException::class.java) {
            employeeService.saveEmployeeSupervisor(employeeSupervisor)
        }.message

        assertEquals(EMPLOYEE_SUPERVISOR_CYCLIC_DEPENDENCY, message)
    }

    @Test
    fun `should throw validation exception when employee name ans supervisor are the same`() {
        val employeeSupervisor = mapOf(
            "Pete" to "Nick",
            "Barbara" to "Nick",
            "Nick" to "Barbara"
        )

        val message = assertThrows(ValidationException::class.java) {
            employeeService.saveEmployeeSupervisor(employeeSupervisor)
        }.message

        assertEquals(EMPLOYEE_SUPERVISOR_CYCLIC_DEPENDENCY, message)
    }
}