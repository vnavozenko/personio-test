package com.personio.test.repository

import com.personio.test.model.domain.EmployeeEntity
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface EmployeeRepository : CrudRepository<EmployeeEntity, Long> {
    fun findAllByNameIn(names: Set<String>): List<EmployeeEntity>

    fun findByName(name: String): EmployeeEntity?

    fun findBySupervisor(supervisor: EmployeeEntity?): EmployeeEntity?
}