package com.personio.test.controller

import com.personio.test.model.MutableMapNodes
import com.personio.test.service.EmployeeService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v1/employee")
class EmployeeController(
    val employeeService: EmployeeService
) {
    @PostMapping
    fun saveEmployeeSupervisor(@RequestBody employeeSupervisor: Map<String, String>): MutableMapNodes {
        employeeService.saveEmployeeSupervisor(employeeSupervisor)

        return employeeService.getAssignedEmployeeBySupervisor()
    }

    @GetMapping("/supervisors/{employeeName}")
    fun getEmployeeWithSupervisorsByName(@PathVariable employeeName: String): MutableMapNodes {
        return employeeService.getEmployeeSupervisors(employeeName)
    }
}
