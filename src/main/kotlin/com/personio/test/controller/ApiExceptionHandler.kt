package com.personio.test.controller

import com.personio.test.model.ValidationException
import com.personio.test.model.response.ApiErrorResponse
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class ApiExceptionHandler : ResponseEntityExceptionHandler() {

    @ExceptionHandler(value = [ValidationException::class])
    protected fun handleValidationExceptions(
        exception: ValidationException?, request: WebRequest?
    ): ResponseEntity<Any> {
        logger.error(exception?.message, exception)

        return handleExceptionInternal(
            exception!!,
            ApiErrorResponse(
                code = HttpStatus.BAD_REQUEST.value(),
                errorMessage = exception.message,
                details = exception.details
            ),
            HttpHeaders(),
            HttpStatus.BAD_REQUEST,
            request!!
        )
    }
}
