package com.personio.test.model.domain

import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.Table

@Entity
@Table(name = "employee")
data class EmployeeEntity(
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long? = null,

    @Column(name = "name", unique = true)
    val name: String,

    @ManyToOne(fetch = FetchType.LAZY, optional = true, cascade = [CascadeType.PERSIST, CascadeType.MERGE])
    @JoinColumn(name = "supervisor_id")
    var supervisor: EmployeeEntity? = null,

    @OneToMany(mappedBy = "supervisor")
    val supervised: List<EmployeeEntity> = listOf()
)