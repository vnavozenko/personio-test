package com.personio.test.model

class ValidationException(message: String, detailsTest: Map<Any, Any>? = null) : RuntimeException(message) {
    val details: Map<Any, Any> = detailsTest ?: emptyMap()
}

class EmployeeNotFoundException(message: String) : RuntimeException(message)

object ErrorMessageCodes {
    const val EMPLOYEE_NOT_FOUND = "EMPLOYEE_NOT_FOUND"
    const val ROOT_SUPERVISOR_NOT_FOUND = "ROOT_SUPERVISOR_NOT_FOUND"
    const val EMPLOYEE_SUPERVISOR_CYCLIC_DEPENDENCY = "EMPLOYEE_SUPERVISOR_CYCLIC_DEPENDENCY"
}
