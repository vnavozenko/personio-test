package com.personio.test.model

typealias MutableMapNodes = MutableMap<String, MutableMap<String, Any>>