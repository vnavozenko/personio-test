package com.personio.test.model.response

data class ApiResponse<T>(
    val data: T? = null,
    val code: Int,
    val message: String? = null
)