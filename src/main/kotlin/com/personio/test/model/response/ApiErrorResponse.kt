package com.personio.test.model.response

data class ApiErrorResponse<T>(
    val details: T? = null,
    val code: Int,
    val errorMessage: String? = null
)
