package com.personio.test.service

import com.personio.test.model.EmployeeNotFoundException
import com.personio.test.model.ErrorMessageCodes.EMPLOYEE_NOT_FOUND
import com.personio.test.model.ErrorMessageCodes.EMPLOYEE_SUPERVISOR_CYCLIC_DEPENDENCY
import com.personio.test.model.MutableMapNodes
import com.personio.test.model.ValidationException
import com.personio.test.model.domain.EmployeeEntity
import com.personio.test.repository.EmployeeRepository
import org.springframework.stereotype.Service

@Service
class EmployeeService(
    val employeeRepository: EmployeeRepository
) {
    fun saveEmployeeSupervisor(employeeSupervisor: Map<String, String>) {
        validateSavingEmployeeSupervisor(employeeSupervisor)

        val employeeNames = (employeeSupervisor.keys + employeeSupervisor.values).toSet()
        val employeesByName = getEmployeesAndAssociateByName(employeeNames).toMutableMap()

        // save employee' supervisors
        employeeSupervisor.values.forEach { supervisorName ->
            val supervisor = employeesByName[supervisorName] ?: EmployeeEntity(name = supervisorName)
            employeeRepository.save(supervisor)

            employeesByName[supervisor.name] = supervisor
        }

        // save employees
        employeeSupervisor.entries.forEach { (employeeName, supervisorName) ->
            val supervisor = employeesByName[supervisorName] ?: EmployeeEntity(name = supervisorName)
            val employee = employeesByName[employeeName] ?: EmployeeEntity(name = employeeName)

            employee.supervisor = supervisor

            employeeRepository.save(employee)

            employeesByName[employee.name] = employee
        }
    }

    fun getAssignedEmployeeBySupervisor(supervisor: EmployeeEntity? = null): MutableMapNodes {
        val rootSupervisor = employeeRepository.findBySupervisor(null)

        return rootSupervisor?.let {
            getEmployeeDataBySupervisor(rootSupervisor, mutableMapOf())
        } ?: mutableMapOf()
    }

    fun getEmployeeSupervisors(employeeName: String): MutableMapNodes {
        return employeeRepository.findByName(employeeName)?.let {
            getEmployeeDataWithSupervisors(it, mutableMapOf())
        } ?: throw EmployeeNotFoundException(EMPLOYEE_NOT_FOUND)
    }

    fun validateSavingEmployeeSupervisor(employeeSupervisor: Map<String, String>) {
        employeeSupervisor.entries.forEach { (employeeName, supervisorName) ->
            if (employeeName == supervisorName) {
                throw ValidationException(
                    message = EMPLOYEE_SUPERVISOR_CYCLIC_DEPENDENCY,
                    detailsTest = mapOf(
                        "employeeName" to employeeName,
                        "supervisorName" to supervisorName
                    )
                )
            }

            if (employeeName == employeeSupervisor[employeeSupervisor[employeeName]]) {
                throw ValidationException(
                    message = EMPLOYEE_SUPERVISOR_CYCLIC_DEPENDENCY,
                    detailsTest = mapOf(
                        "employeeName" to employeeName,
                        "supervisorName" to supervisorName
                    )
                )
            }
        }
    }

    private fun getEmployeeDataBySupervisor(supervisor: EmployeeEntity, data: MutableMapNodes): MutableMapNodes {
        data[supervisor.name] = mutableMapOf()

        supervisor.supervised.forEach {
            data[supervisor.name]?.putAll(getEmployeeDataBySupervisor(it, mutableMapOf()))
        }

        return data
    }

    private fun getEmployeeDataWithSupervisors(employee: EmployeeEntity, data: MutableMapNodes): MutableMapNodes {
        data[employee.name] = mutableMapOf()

        if (employee.supervisor != null) {
            data[employee.name]?.putAll(getEmployeeDataWithSupervisors(employee.supervisor!!, mutableMapOf()))
        }

        return data
    }

    private fun getEmployeesAndAssociateByName(names: Set<String>): Map<String, EmployeeEntity> {
        return if (names.isNotEmpty()) {
            employeeRepository.findAllByNameIn(names)
                .associateBy { it.name }
        } else {
            mapOf()
        }
    }
}