# Getting Started

### Technologies

- Kotlin
- Spring Boot
- Spring Data
- Spring Security
- JUnit
- database(Postgres)

### Local environment
We use docker compose for all local dependencies

Run:
```bash
docker-compose up postgres -d
```

#### Run application
```bash
./gradlew bootRun
```

### Default admin login and password
```
username: admin
password: admin
```

### Postman collection for testing
`./Personio - Test.postman_collection.json`

Please import the collection into local Postman application.
